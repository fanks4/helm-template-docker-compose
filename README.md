# Helm Template Docker Compose

## Description
This is a little POC that renders a docker-compose using helm.
A [docker-compose.yaml](https://rafrasenberg.com/docker-compose-traefik-v2/#docker-composeyml) for Traefik was used as example.

## Installation
Please follow the [Installing Helm](https://helm.sh/docs/intro/install/)

## Usage
You can use the helm template function to render the templates using the defaults.
```
cd /path/to/project/docker-compose
helm template .
```
You can add a custom values file to overwrite the default values.
```
cd /path/to/project/docker-compose
helm template . -f my-values.yaml
```

## Support
Here is where to get support for the used applications. I can not provide further support for now but added this point to the roadmap.

### Helm
Please consider [Helm Docs](https://helm.sh/docs/) for additional information about Helm.
